<div class="home-grid-controls">
 
    <?php
//    $tax_arg = array(
//        'post_type' => 'pg-case-studies',
//    );
//    $wp_tax_query1 = new WP_Query($tax_arg);
//    $tax_counter1 = 0;
//    $terms1 = get_terms('segments');
//    if (!empty($terms1) && !is_wp_error($terms1)) {
//        echo "<ul id=\"segments\" class=\"pg-tag-cloud option-set\">";
//        echo '<li class="reset"><a id="reset-btn" class="selected" href="#filter" title="Show All" data-filter="*">All</a></li>';
//        foreach ($terms1 as $term1) {
//            echo '<li><a href="javascript:void(0)" class="cs-tag' . $tax_counter1 . '" title="' . strtoupper(str_replace('-', ' ', $term1->slug)) . '" data-filter=".' . str_replace(' ', '-', $term1->name) . '">' . $term1->name . '</a></li>';
//        }
//        echo '</ul>';
//    }
    ?>
</div>
<div class="row">
    <div class="cs-box cs-grid-wrapper">
        <div class="grid home">
            <div class="gutter-sizer"></div>
            <?php
            $blog_arg = array(
                'post_type' => 'pg-case-studies',
                'orderby' => 'post_date',
                'order' => 'date',
                'posts_per_page' => 8,
                'post_status' => 'publish',
            );
            $wp_blog_query = new WP_Query($blog_arg);
            $postx_counter = -1;
            if (have_posts()) :
                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                    $postx_counter++;
                    ?>
                    <?php
                    $tags = wp_get_post_tags($post->ID);
                    $total = count($tags);
                    $tag_count = 0;
                    $post_tags = get_tags(array('orderby' => 'count', 'order' => 'DESC'));
                    $terms = get_the_terms(get_the_ID(), 'cities');
                    $terms_a = get_the_terms(get_the_ID(), 'segments');
                
                    $a = "";
                    $aa = "";

                    $b = "";
                    $bb = "";
                    foreach ((array) $terms_a as $tt_a) {
                        $b.= str_replace(' ', '-', $tt_a->name) . ' ';
                    }
                    foreach ((array) $terms as $tt) {
                        $a.= str_replace(' ', '-', $tt->name) . ' ';
                    }

                    if ($terms) {
                        foreach ($terms as $ttt) {
                            $tag_count++;
                            $aa.= str_replace(' ', '-', $ttt->name) . ' ';
                        }
                    }

                    if ($terms_a) {
                        foreach ($terms_a as $ttt_a) {
                            $bb.= str_replace(' ', '-', $ttt_a->name) . ' ';
                        }
                    }
                    $tile_Image_size = "";
                    if ($postx_counter % 2 === 0) {
                        $tile_Image_size = "twentyfourteen-480x480";
                    } else {
                        $tile_Image_size = "twentyfourteen-480x480";
                    }
                    ?> 
                    <div class="grid-item isotope-item <?php echo $aa; ?> <?php echo $bb; ?>" data-count="<?php echo $postx_counter; ?>">

                        <div class="image">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                <?php if (has_post_thumbnail()) { ?>
                                    <?php the_post_thumbnail($tile_Image_size, array('class' => "img-responsive")); ?>
                                <?php } else { ?>
                                    <img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=480%C3%97480&w=480&h=480" alt="Placeholder Image" class="img-responsive" />
                                <?php } ?>
                            </a>
                        </div>
                        <div class="content">
<!--                            <a class="desktop-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read</a> -->
                            <a class="desktop-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">&nbsp;</a>
                            <div class="inner-content">
                               
                                <div class="grid-item-title"><a class="title-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
                                <?php
                                if ($terms && !is_wp_error($terms)):
//                                    echo "<ul id=\"filters\" class=\"cs-tax-name\">";
//                                    foreach ($terms as $term) {
//                                        $tag_count++;
//                                        echo '<li class="cat-item"><a href="#filter" data-filter=".' . str_replace(' ', '-', $term->name) . '">' . $term->name . '</a>';
//                                        if ($tag_count) {
//                                            echo'<span class="comma">,</span></li>';
//                                        }
//                                    }
//                                    echo "</ul>";
                                    ?>
                                <?php endif; ?>

                                <?php
                                if ($terms_a) {
                                    echo '<ul class="gridItemFilter cat-item-set cs-tax-name">';
                                    foreach ($terms_a as $tag) {
                                        $tag_count++;

                                        echo '<li class="cat-item"><a href="/segments/'.$tag->slug.'" title="Tagged: '.$tag->name.'" data-filter=".' . str_replace(' ', '-', $tag->name) . '">' . $tag->name . '</a>';
                                        if ($tag_count) {
                                            echo'<span class="comma">,</span></li>';
                                        }
                                    }

                                    echo '</ul>';
                                }
                                ?> 
                                 <a class="learn-more" role="button" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read</a>   
                            </div>
                        </div>
                         
                        
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>

</div>