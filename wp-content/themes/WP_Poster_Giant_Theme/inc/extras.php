<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_page_menu_args($args) {
    $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'pixelfire_page_menu_args');

/**
 * Adds custom classes to the array of body classes.
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_body_classes($classes) {
    // Adds a class of group-blog to blogs with more than 1 published author
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    return $classes;
}

add_filter('body_class', 'pixelfire_body_classes');

/**
 * Filter in a link to a content ID attribute for the next/previous image links on image attachment pages
 *
 * @since WP PixelFire Theme 1.0
 */
function pixelfire_enhanced_image_navigation($url, $id) {
    if (!is_attachment() && !wp_attachment_is_image($id))
        return $url;

    $image = get_post($id);
    if (!empty($image->post_parent) && $image->post_parent != $id)
        $url .= '#main';

    return $url;
}

add_filter('attachment_link', 'pixelfire_enhanced_image_navigation', 10, 2);

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @since WP PixelFire Theme 1.1
 */
function pixelfire_wp_title($title, $sep) {
    global $page, $paged;

    if (is_feed())
        return $title;

    // Add the blog name
    $title .= get_bloginfo('name');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && ( is_home() || is_front_page() ))
        $title .= " $sep $site_description";

    // Add a page number if necessary:
    if ($paged >= 2 || $page >= 2)
        $title .= " $sep " . sprintf(__('Page %s', 'pixelfire'), max($paged, $page));

    return $title;
}

add_filter('wp_title', 'pixelfire_wp_title', 10, 2);

if (!function_exists('postergiant_primary_menu()')) :

    function postergiant_primary_menu() {
        // display the WordPress Custom Menu if available

        wp_nav_menu(array(
            'menu' => 'primary',
            'theme_location' => 'primary',
            'menu_id' => 'js-navigation-menu',
            'depth' => 2,
            'container' => null,
            'container_class' => null,
            'menu_class' => 'nav-wrapper',
            'items_wrap' => '<nav class="%2$s" role="navigation" ><ul id="%1$s" class="navigation-menu show">%3$s</ul></nav>',
                //'fallback_cb'       => 'wp_page_menu',
                //'walker'            => new wp_bootstrap_navwalker()
        ));
    }

/* end header menu */
endif;

if (!function_exists('postergiant_social_menu()')) :

// display the Social Icon Menu if available   
    function postergiant_social_menu() {
        ?>
        <ul class="social-list">
            <li><a href="https://www.facebook.com/posterGIANTinc" title="facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/postergiant/" title="instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://plus.google.com/114085159533063555983" title="Google+" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="https://www.linkedin.com/company/postergiant" title="LinkedIn" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/postergiant" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="https://www.youtube.com/user/postergiant" title="YouTube" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
        </ul>
        <?
    }
endif;


