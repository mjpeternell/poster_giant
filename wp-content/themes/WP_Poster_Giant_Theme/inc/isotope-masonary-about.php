<div class="row">
    <div class="cs-box cs-grid-wrapper">
        <div class="grid about">
            <div class="gutter-sizer"></div>
            <?php
            $blog_arg = array(
                'post_type' => 'staff',
                'orderby' => 'post_date',
                'order' => 'date',
                'posts_per_page' => -1,
                'post_status' => 'publish',
            );
            $wp_blog_query = new WP_Query($blog_arg);
            $bio_counter = -1;
            if (have_posts()) :
                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                    $bio_counter++;
                    ?>
                    <?php
                    $tags = wp_get_post_tags($post->ID);
                    $total = count($tags);
                    $tag_count = 0;
                    $post_tags = get_tags(array('orderby' => 'count', 'order' => 'DESC'));
                    ?> 
                    <div class="grid-item isotope-item">
                        <a class="learn-more" id="teamBioImg-<?php echo $bio_counter; ?>" role="button" href="javascript:void(0)" title="<?php the_title(); ?>">View Bio</a> 
                        <div class="image">

                            <?php if (has_post_thumbnail()) { ?>
                                <?php the_post_thumbnail('twentyfourteen-680x680', array('class' => "img-responsive")); ?>
                            <?php } else { ?>
                                <img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=1323%C3%97914&w=1323&h=914" class="img-responsive" />
                            <?php } ?>

                        </div>
                        <a class="desktop-link" id="teamBioLink-<?php echo $bio_counter; ?>" href="javascript:void(0)" title="<?php the_title(); ?>">
                            <div class="content"  id="<?php echo 'myBio-content' . $bio_counter; ?>">

                                <div class="inner-content">
                                    <div class="grid-item-title">
                                        <?php the_title(); ?>
                                        <?php
                                        if (get_field('staff_title')) {
                                            echo '<span class="grid-item-job-title">' . get_field('staff_title') . '</span>';
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    echo '<div class="grid-item-job-excerpt">';
                                    the_excerpt();
                                    echo '</div>';
                                    ?>
                                </div>

                            </div>
                        </a>

                    </div>
                    <?php
                endwhile;
            endif;
            ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>

</div>