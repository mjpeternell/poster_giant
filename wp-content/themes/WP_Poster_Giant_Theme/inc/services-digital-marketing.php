<?php
/*
 * Digital Marketing section of Our Services Page
 */
?>
<?php
$dm_page_anchor = "";
if (get_field('dm_page_anchor')) {
    $dm_page_anchor = get_field('dm_page_anchor');
}
?>

<div id="<?php echo $dm_page_anchor; ?>" class="row parent">
    <div class="col-product-6 entry-copy"> 
        <header class="entry-header">
            <?php
            if (get_field('dm_subtitle')) {
                echo '<h1 class="entry-title">' . get_field('dm_title') . '</h1>';
            }
            ?>
            <?php
            if (get_field('dm_subtitle')) {
                echo '<h2 class="entry-subtitle">' . get_field('dm_subtitle') . '</h2>';
            }
            ?>
        </header><!-- .entry-header -->
        <div class="entry-content">
            <?php
            if (get_field('dm_main_copy')) {
                echo get_field('dm_main_copy');
            }
            ?>
        </div><!-- .entry-content -->
    </div>
    <div class="col-product-6 ">
                <?php
        $image = get_field('dm_image');

        if (!empty($image)):
            // vars
            $url = $image['url'];
            $title = $image['title'];
            $alt = $image['alt'];
            $caption = $image['caption'];

            // thumbnail
            $size = 'twentyfourteen-680x680';
            $thumb = $image['sizes'][$size];
            $width = $image['sizes'][$size . '-width'];
            $height = $image['sizes'][$size . '-height'];
            ?>

            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" class="img-responsive"/>

        <?php endif; ?>
    </div>
</div>
<div class="row parent-2">
    <div class="col-product-6 shift-right"> 
        <div class="accordion">
            <dl>
                <ul class="services-list">
                    <?php
// check if the repeater field has rows of data
                    if (have_rows('dm_repeater_types')):

                        // loop through the rows of data
                        $accord_count = 0;
                        while (have_rows('dm_repeater_types')) : the_row();
                            $accord_count++;
                            ?>

                            <dt>
                                <a href="<?php echo '#accordion' . $accord_count; ?>" aria-expanded="false" aria-controls="accordion<?php echo '#accordion' . $accord_count; ?>" class="accordion-title accordionTitle js-accordionTrigger"><?php echo get_sub_field('title'); ?></a>
                            </dt>
                            <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                                <?php
                                echo '<div class="sevice-tagline"><h3>' . get_sub_field('tagline') . '</h3></div>';
                                echo '<div class="sevice-copy">' . get_sub_field('copy') . '</div>';
                                ?>
                                <?php
                                echo '<div class="sevice-photos">';
                                $image_1 = get_sub_field('image_1');
                                $image_2 = get_sub_field('image_2');
                                $image_3 = get_sub_field('image_3');
                                $image_4 = get_sub_field('image_4');

                                if (!empty($image_1)):

                                    // vars
                                    $url = $image_1['url'];
                                    $title = $image_1['title'];
                                    $alt = $image_1['alt'];
                                    $title = $image_1['title'];

                                    // thumbnail
                                    $size = 'thumbnail';
                                    $size_pop = $image_1['sizes']['twentyfourteen-680x680'];
                                    $thumb = $image_1['sizes'][$size];
                                    $width = $image_1['sizes'][$size . '-width'];
                                    $height = $image_1['sizes'][$size . '-height'];
                                    ?>
                                    <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                        <span class="expand-hint"><i class="fa fa-expand" aria-hidden="true"></i></span>
                                        <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                    </a>
                                    <?php
                                endif;

                                if (!empty($image_2)):

                                    // vars
                                    $url = $image_2['url'];
                                    $title = $image_2['title'];
                                    $alt = $image_2['alt'];
                                    $title = $image_2['title'];

                                    // thumbnail
                                    $size = 'thumbnail';
                                    $size_pop = $image_2['sizes']['twentyfourteen-680x680'];
                                    $thumb = $image_2['sizes'][$size];
                                    $width = $image_2['sizes'][$size . '-width'];
                                    $height = $image_2['sizes'][$size . '-height'];
                                    ?>
                                    <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">    
                                        <span class="expand-hint"><i class="fa fa-expand" aria-hidden="true"></i></span>
                                        <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                    </a>
                                    <?php
                                endif;
                                if (!empty($image_3)):

                                    // vars
                                    $url = $image_3['url'];
                                    $title = $image_3['title'];
                                    $alt = $image_3['alt'];
                                    $title = $image_3['title'];

                                    // thumbnail
                                    $size = 'thumbnail';
                                    $size_pop = $image_3['sizes']['twentyfourteen-680x680'];
                                    $thumb = $image_3['sizes'][$size];
                                    $width = $image_3['sizes'][$size . '-width'];
                                    $height = $image_3['sizes'][$size . '-height'];
                                    ?>
                                    <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                        <span class="expand-hint"><i class="fa fa-expand" aria-hidden="true"></i></span>
                                        <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                    </a>
                                    <?php
                                endif;
                                if (!empty($image_4)):

                                    // vars
                                    $url = $image_4['url'];
                                    $title = $image_4['title'];
                                    $alt = $image_4['alt'];
                                    $title = $image_4['title'];

                                    // thumbnail
                                    $size = 'thumbnail';
                                    $size_pop = $image_4['sizes']['twentyfourteen-680x680'];
                                    $thumb = $image_4['sizes'][$size];
                                    $width = $image_4['sizes'][$size . '-width'];
                                    $height = $image_4['sizes'][$size . '-height'];
                                    ?>
                                    <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                        <span class="expand-hint"><i class="fa fa-expand" aria-hidden="true"></i></span>
                                        <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                    </a>
                                    <?php
                                endif;
                                echo '</div>';
                                ?>
                            </dd>
                            <?php
                        endwhile;

                    else :

                    // no rows found

                    endif;
                    ?>
                </ul>
            </dl>
        </div>
    </div>
</div>