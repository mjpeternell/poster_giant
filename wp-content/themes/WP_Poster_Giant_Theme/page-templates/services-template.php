<?php
/**
 * Template Name: Services Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if (has_post_thumbnail($post->ID)):
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    $my_bground = 'style="background-image: url(\'' . $image[0] . '\')"';
                endif;
                ?>
                <div class="hero-subpages hero-bg" <?php echo $my_bground; ?>>
                    <div class="hero-img-wrapper">
                        <?php //postergiant_post_thumbnail(); ?>
                    </div>
                </div>

                <div class="entry-content">
                    <?php //the_content(); ?>
                    <?php //wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                    <?php //edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                </div><!-- .entry-content -->
            <?php endwhile; ?>
            <section id="servicesList" class="sections section-services-list">
                <div class="row">
                    <header class="entry-header col-md-12">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header><!-- .entry-header -->
                </div>
                <div class="row">
                    <div class="sl-box col-md-12">
                        <?php
                        //$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                        $blog_arg = array(
                            'post_type' => 'services',
                            'orderby' => 'post_date',
                            'order' => 'date',
                            'post_status' => 'publish',
                            'posts_per_page' => 12,
                                //'paged' => $paged,
                        );
                        $wp_blog_query = new WP_Query($blog_arg);
                        $postx_counter = -1;
                        if (have_posts()) :
                            while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                                $postx_counter++;
                                ?>
                                <div class="parent row">
                                    <div class="col-md-6 content">
                                        <h2><?php the_title(); ?></h2>
                                        <?php
                                        if (get_field('post_subtitle')) {
                                            echo '<h3 class="entry-subtitle">' . get_field('post_subtitle') . '</h3>';
                                        }
                                        ?>
                                        <div class="entry-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 image">
                                        <?php if (has_post_thumbnail()) { ?>
                                            <?php the_post_thumbnail('', array('class' => "img-responsive")); ?>
                                        <?php } else { ?>
                                            <img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=1323%C3%97914&w=1323&h=914" class="img-responsive" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        ?>

                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </section>
        </article>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();  ?>
<?php get_footer(); ?>