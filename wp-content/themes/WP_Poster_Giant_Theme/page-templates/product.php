<?php
/**
 * Template Name: Product
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php $themeLink = get_stylesheet_directory_uri(); ?>
        <?php if (is_page()) : ?>
            <div class = "single-bg">
            <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
            </div>
        <?php endif; ?>
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="fluid-container">
                    <div class="row parent">
                        <div class="col-product-6"> 
                            <?php
                            //postergiant_post_thumbnail();
                            the_post_thumbnail('twentyfourteen-680x680', array('class' => 'img-responsive'));
                            ?> 
                        </div>
                        <div class="col-product-6 entry-copy">
                            <header class="entry-header">
                                <h1 class="entry-title"><?php the_title(); ?></h1>
                                <?php
                                if (get_field('page_subtitle')) {
                                    echo '<h2>' . get_field('page_subtitle') . '</h2>';
                                }
                                ?>
                                <h2 class="entry-subtitle"><?php
                                    if (get_field('page_tagline')) {
                                        echo get_field('page_tagline');
                                    }
                                    ?></h2>
                            </header><!-- .entry-header -->
                            <div class="entry-content">
                                <?php the_content(); ?>
                                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                                <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                            </div><!-- .entry-content -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-product-6"> 
                            <div class="accordion">
                                <dl>
                                    <ul class="services-list">
                                        <?php
// check if the repeater field has rows of data
                                        if (have_rows('type_of_services')):

                                            // loop through the rows of data
                                            $accord_count = 0;
                                            while (have_rows('type_of_services')) : the_row();
                                                $accord_count++;
                                                ?>

                                                <dt>
                                                    <a href="<?php echo '#accordion' . $accord_count; ?>" aria-expanded="false" aria-controls="accordion<?php echo '#accordion' . $accord_count; ?>" class="accordion-title accordionTitle js-accordionTrigger"><?php echo get_sub_field('title'); ?></a>
                                                </dt>
                                                <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                                                    <?php
                                                    echo '<div class="sevice-tagline"><h3>' . get_sub_field('tagline') . '</h3></div>';
                                                    echo '<div class="sevice-copy">' . get_sub_field('copy') . '</div>';
                                                    ?>
                                                    <?php
                                                    echo '<div class="sevice-photos">';
                                                    $image_1 = get_sub_field('image_1');
                                                    $image_2 = get_sub_field('image_2');
                                                    $image_3 = get_sub_field('image_3');
                                                    $image_4 = get_sub_field('image_4');

                                                    if (!empty($image_1)):

                                                        // vars
                                                        $url = $image_1['url'];
                                                        $title = $image_1['title'];
                                                        $alt = $image_1['alt'];
                                                        $title = $image_1['title'];

                                                        // thumbnail
                                                        $size = 'thumbnail';
                                                        $size_pop = $image_1['sizes']['twentyfourteen-680x680'];
                                                        $thumb = $image_1['sizes'][$size];
                                                        $width = $image_1['sizes'][$size . '-width'];
                                                        $height = $image_1['sizes'][$size . '-height'];
                                                        ?>
                                                        <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                                        </a>
                                                        <?php
                                                    endif;

                                                    if (!empty($image_2)):

                                                        // vars
                                                        $url = $image_2['url'];
                                                        $title = $image_2['title'];
                                                        $alt = $image_2['alt'];
                                                        $title = $image_2['title'];

                                                        // thumbnail
                                                        $size = 'thumbnail';
                                                        $size_pop = $image_2['sizes']['twentyfourteen-680x680'];
                                                        $thumb = $image_2['sizes'][$size];
                                                        $width = $image_2['sizes'][$size . '-width'];
                                                        $height = $image_2['sizes'][$size . '-height'];
                                                        ?>
                                                        <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">    
                                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                                        </a>
                                                        <?php
                                                    endif;
                                                    if (!empty($image_3)):

                                                        // vars
                                                        $url = $image_3['url'];
                                                        $title = $image_3['title'];
                                                        $alt = $image_3['alt'];
                                                        $title = $image_3['title'];

                                                        // thumbnail
                                                        $size = 'thumbnail';
                                                        $size_pop = $image_3['sizes']['twentyfourteen-680x680'];
                                                        $thumb = $image_3['sizes'][$size];
                                                        $width = $image_3['sizes'][$size . '-width'];
                                                        $height = $image_3['sizes'][$size . '-height'];
                                                        ?>
                                                        <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                                        </a>
                                                        <?php
                                                    endif;
                                                    if (!empty($image_4)):

                                                        // vars
                                                        $url = $image_4['url'];
                                                        $title = $image_4['title'];
                                                        $alt = $image_4['alt'];
                                                        $title = $image_4['title'];

                                                        // thumbnail
                                                        $size = 'thumbnail';
                                                        $size_pop = $image_4['sizes']['twentyfourteen-680x680'];
                                                        $thumb = $image_4['sizes'][$size];
                                                        $width = $image_4['sizes'][$size . '-width'];
                                                        $height = $image_4['sizes'][$size . '-height'];
                                                        ?>
                                                        <a href="#modal" name="modal" data-img-title="<?php echo $title; ?>" data-pop-img="<?php echo $size_pop; ?>">
                                                            <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                                                        </a>
                                                        <?php
                                                    endif;
                                                    echo '</div>';
                                                    ?>
                                                </dd>
                                                <?php
                                            endwhile;

                                        else :

                                        // no rows found

                                        endif;
                                        ?>
                                    </ul>
                                </dl>
                            </div>
                        </div>
                        <div class="col-product-6">
                            <div class="page-quote">
                                <div class="quote-outer">
                                    <?php
                                    if (get_field('page_quote')) {
                                        echo '<div class="quote-inner">';
                                        echo '<i class="fa fa-quote-left" aria-hidden="true"></i>';
                                        echo get_field('page_quote');
                                        echo '<i class="fa fa-quote-right" aria-hidden="true"></i>';
                                        echo '</div>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="service-product-links">
                                <div class="spl-menu">

                                    <?php if (!is_page('digital')) { ?>
                                        <div class="spl-box"><a class="hov" href="/our-services/digital" title="Digital Marketing"><span>Digital Marketing</span></a></div>
                                    <?php } ?>
                                    <?php if (!is_page('guerilla')) { ?>
                                        <div class="spl-box"><a class="hov" href="/our-services/guerilla" title="Gorilla Marketing"><span>Guerilla Marketing</span></a></div>
                                    <?php } ?>
                                    <?php if (!is_page('public-relations')) { ?>
                                        <div class="spl-box"><a class="hov" href="/our-services/public-relations" title="Public Relations"><span>Public Relations</span></a></div>
                                    <?php } ?>
                                    <?php if (!is_page('product')) { ?>
                                        <div class="spl-box"><a class="hov" href="/our-services/product" title="Product Marketing"><span>Product Marketing</span></a></div>
                                    <?php } ?>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article><!-- #post-<?php the_ID(); ?> -->
            <?php
// End the Loop
        endwhile;
        ?>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/modal-popup'); ?>

<?php get_footer(); ?>