<?php
/**
 * Template Name: Case Studies Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if (has_post_thumbnail($post->ID)):
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    $my_bground = 'style="background-image: url(\'' . $image[0] . '\')"';
                endif;
                ?>
                <div class="hero-subpages hero-bg" <?php echo $my_bground; ?>>
                    <div class="hero-img-wrapper">
                        <?php //postergiant_post_thumbnail(); ?>
                    </div>
                </div>

                <div class="entry-content">
                    <?php //the_content(); ?>
                    <?php //wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                    <?php //edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                </div><!-- .entry-content -->
            <?php endwhile; ?>
            <section id="servicesList" class="sections section-cs-list">
                <div class="row">
                    <header class="entry-header cs">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <button id="mobileFilterBtn" class="btn btn-default btn-sm" title="Filter" role="button">Filter</button>
                    </header><!-- .entry-header -->
                </div>
                <div class="row">
                    <div class="cs-box cs-grid-wrapper">
                        <div class="grid case">
                            <div class="gutter-sizer"></div>
                            <?php
                            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                            $blog_arg = array(
                                'post_type' => 'pg-case-studies',
                                'orderby' => 'post_date',
                                'order' => 'date',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'paged' => $paged,
                            );
                            $wp_blog_query = new WP_Query($blog_arg);
                            $postx_counter = -1;
                            if (have_posts()) :
                                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                                    $postx_counter++;
                                    ?>
                                    <?php
                                    $tags = wp_get_post_tags($post->ID);
                                    $total = count($tags);
                                    $tag_count = 0;
                                    $post_tags = get_tags(array('orderby' => 'count', 'order' => 'DESC'));
                                    $terms = get_the_terms(get_the_ID(), 'cities');
                                    $terms_a = get_the_terms(get_the_ID(), 'segments');
                                    
                                    $a = "";
                                    $aa = "";
                                    
                                    $b = "";
                                    $bb = "";
                                    foreach ((array) $terms_a as $tt_a) {
                                        $b.= str_replace(' ', '-', $tt_a->name) . ' ';
                                    }
                                    foreach ((array) $terms as $tt) {
                                        $a.= str_replace(' ', '-', $tt->name) . ' ';
                                    }
                                    
                                    if ($terms) {
                                        foreach ($terms as $ttt) {
                                            $tag_count++;
                                            $aa.= str_replace(' ', '-', $ttt->name) . ' ';
                                        }
                                    }
                                    
                                    if ($terms_a) {
                                        foreach ($terms_a as $ttt_a) {
                                            $bb.= str_replace(' ', '-', $ttt_a->name) . ' ';
                                        }
                                    }
                                    ?> 
                                    <div class="grid-item isotope-item <?php echo $aa; ?> <?php echo $bb; ?>">

                                        <div class="image">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                                <?php if (has_post_thumbnail()) { ?>
                                                    <?php the_post_thumbnail('twentyfourteen-768x515', array('class' => "img-responsive")); ?>
                                                <?php } else { ?>
                                                    <img src="https://placeholdit.imgix.net/~text?txtsize=33&txt=1323%C3%97914&w=1323&h=914" alt="Placeholder Img" class="img-responsive" />
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="content">

                                            <div class="grid-item-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></div>
                                            <?php
                                            if ($terms && !is_wp_error($terms)):
                                                echo '<ul class="cat-item-set cs-tax-name">';
                                                foreach ($terms as $term) {
                                                    $tag_count++;
                                                    echo '<li class="cat-item"><a href="#" data-filter=".' . str_replace(' ', '-', $term->name) . '">' . $term->name . '</a>';
                                                    if ($tag_count) {
                                                        echo'<span class="comma">,</span></li>';
                                                    }
                                                }
                                                echo "</ul>";
                                                ?>
                                            <?php endif; ?>
                                            <?php
                                            if (get_field('post_subtitle')) {
                                                echo '<h3 class="entry-subtitle">' . get_field('post_subtitle') . '</h3>';
                                            }
                                            ?>

                                            <?php
                                            if ($tags) {
                                                echo '<ul class="cat-item-set cs-tax-name">';
                                                foreach ($tags as $tag) {
                                                    $tag_count++;

                                                    echo '<li class="cat-item"><a href="#filter" data-filter=".' . str_replace(' ', '-', $tag->name) . '">' . $tag->name . '</a>';
                                                    if ($tag_count) {
                                                        echo'<span class="comma">,</span></li>';
                                                    }
                                                }

                                                echo '</ul>';
                                            }
                                            ?> 

                                            <div class="entry-content">
                                                <?php the_excerpt(); ?>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                    <div class="cs-col-md-3 grid-controls">
                        <h2>Cities</h2>
                        <?php
                        $tax_arg = array(
                            'post_type' => 'pg-case-studies',
                        );
                        $wp_tax_query = new WP_Query($tax_arg);
                        $tax_counter = 0;
                        $terms = get_terms('cities');
                        if (!empty($terms) && !is_wp_error($terms)) {
                            echo "<ul id=\"cities\" class=\"pg-tag-cloud option-set\">";
                            echo '<li class="reset"><a id="reset-btn" class="selected" href="#filter" title="Show All" data-filter="*">Show All</a><li>';
                            foreach ($terms as $term) {
                                echo '<li><a href="#" class="cs-tag' . $tax_counter . '" title="' . strtoupper(str_replace('-', ' ', $term->slug)) . '" data-filter=".' . str_replace(' ', '-', $term->name) . '">' . $term->name . '</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>
                        <br><br>
                         <h2>Segments</h2>
                        <?php
                        $wp_tax_query1 = new WP_Query($tax_arg);
                        $tax_counter1 = 0;
                        $terms1 = get_terms('segments');
                        if (!empty($terms1) && !is_wp_error($terms1)) {
                            echo "<ul id=\"segments\" class=\"pg-tag-cloud option-set\">";
                            echo '<li class="reset"><a id="reset-btn" class="selected" href="#filter" title="Show All" data-filter="*">Show All</a><li>';
                            foreach ($terms1 as $term1) {
                                echo '<li><a href="#" class="cs-tag' . $tax_counter1 . '" title="' . strtoupper(str_replace('-', ' ', $term1->slug)) . '" data-filter=".' . str_replace(' ', '-', $term1->name) . '">' . $term1->name . '</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>
                    </div>
                </div>
            </section>
        </article>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();         ?>
<?php get_footer(); ?>