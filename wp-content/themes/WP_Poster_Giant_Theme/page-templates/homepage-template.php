<?php
/**
 * Template Name: Home Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                $thumb_id = get_post_thumbnail_id();
                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                $thumb_url = $thumb_url_array[0];
                ?>
                <div class="hero parallax-window" data-parallax="scroll" data-image-src="<?php echo $thumb_url; ?>">
                    <?php //postergiant_post_thumbnail(); ?>
                    <header class="entry-header ">
                        <h1 class="entry-title"><a href="/contact"><?php the_title(); ?></a></h1>
                        <?php
                        if (get_field('page_subtitle')) {
                            echo '<h2 class="entry-subtitle">' . get_field('page_subtitle') . '</h2>';
                        }
                        ?>
                    </header><!-- .entry-header -->
                </div>
                <div class="entry-content">
                    <?php //the_content(); ?>
                    <?php //wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                    <?php //edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>');  ?>
                </div><!-- .entry-content -->
                <section id="intro" class="sections section-intro">
                    <div class="row">
                        <div class="col-md-5">
                            <?php
                            if (get_field('home_page_intro')) {
                                echo '<div class="white-box"><div class="inner-box">' . get_field('home_page_intro') . '</div></div>';
                            }
                            ?>
                        </div>
                        <div class="col-md-7">
                            <?php
                            $image_a = get_field('home_page_intro_image');

                            if (!empty($image_a)):

                                // vars
                                $url_a = $image_a['url'];
                                $title_a = $image_a['title'];
                                $alt_a = $image_a['alt'];
                                $caption_a = $image_a['caption'];

                                // thumbnail
                                $size_a = 'thumbnail';
                                $thumb_a = $image_a['sizes'][$size_a];
                                $width_a = $image_a['sizes'][$size_a . '-width'];
                                $height_a = $image_a['sizes'][$size_a . '-height'];
                                ?>
                                <img src="<?php echo $url_a; ?>" alt="<?php echo $alt_a; ?>"  class="img-responsive" />
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <section id="ourServices" class="sections cta section-our-services ">
                    <div class="row">
                        <?php
                        if (get_field('our_services_section_title')) {
                            echo '<h2 class="section-title">' . get_field('our_services_section_title') . '</h2>';
                        }
                        ?>
                        <?php
                        // check if the repeater field has rows of data
                        if (have_rows('our_services_repeater')):
                            // loop through the rows of data
                            while (have_rows('our_services_repeater')) : the_row();
                                // display a sub field value
                                $link = get_sub_field('url');
                                $label = get_sub_field('label');
                                echo '<div class="os-box col-md-3"><a class="hov" href="' . $link . '" title="' . $label . '"><span>' . $label . '</span></a></div>';
                            endwhile;
                        else :
                        // no rows found
                        endif;
                        ?>
                    </div>
                </section>
                <section id="ourWork" class="sections section-our-work">
                    <div class="row">
                        <div class="col-md-7">

                            <div class="img-our-work">
                                <?php get_template_part('inc/youtube-embed-home'); ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <?php
                            if (get_field('home_page_our_work')) {
                                echo '<div>' . get_field('home_page_our_work') . '</div>';
                            }
                            ?>
                        </div>
                    </div>
                </section>
            <?php endwhile; ?>
            <section id="ourTestimonials" class="sections cta section-testimonials">
                <h2 class="section-title">WHAT OUR CLIENTS SAY</h2>
                <ul class="bxslider testimonial-slider">
                    <?php if (have_rows('testimonial_repeater')): ?>

                        <?php
                        while (have_rows('testimonial_repeater')): the_row();

                            // vars
                            $image = get_sub_field('testimonial_source');
                            $testimonial_source = get_sub_field('testimonial_source');
                            $testimonial_content = get_sub_field('testimonial_content');
                            $link_label = get_sub_field('hero_cta_buttton_label');
                            $link = get_sub_field('hero_cta_buttton_link');
                            //print_r($image);
                            ?>

                            <li class="slide">
                                <?php if ($testimonial_content): ?>
                                    <div class="caption">
                                        <?php echo '<i class="fa fa-quote-left" aria-hidden="true"></i> ' . $testimonial_content . ' <i class="fa fa-quote-right" aria-hidden="true"></i>'; ?>            
                                    </div>
                                <?php endif; ?>
                                <div class="caption-lower">
                                    <?php if ($testimonial_source): ?>
                                        <?php echo $testimonial_source; ?>
                                    <?php endif; ?>
                                </div>
                            </li>

                        <?php endwhile; ?>



                    <?php endif; ?>
                </ul>
            </section>
            <section id="caseStudies" class="sections cs section-case-studies">
                <h2 class="section-title">CASE STUDIES</h2>
                <?php get_template_part('inc/isotope-masonary-home'); ?>
            </section>
        </article>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();  ?>
<?php get_footer(); ?>