<?php
/**
 * Template Name: Our Services Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                if (has_post_thumbnail($post->ID)):
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    $my_bground = 'style="background-image: url(\'' . $image[0] . '\')"';
                endif;
                ?>
                <div class="hero-subpages hero-bg" <?php echo $my_bground; ?>>
                    <div class="hero-img-wrapper">
                        <?php //postergiant_post_thumbnail(); ?>
                    </div>
                </div>

                <div class="entry-content">
                    <?php //the_content(); ?>
                    <?php //wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                    <?php //edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                </div><!-- .entry-content -->
            <?php endwhile; ?>
            <section id="servicesList" class="sections section-services-list">
                <div class="row">
                    <header class="entry-header cs">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                        <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                    </header><!-- .entry-header -->
                </div>
            </section>
            <div class="fluid-container">
                <?php get_template_part('inc/services-design'); ?>
                <?php get_template_part('inc/services-digital-marketing'); ?>
                <?php get_template_part('inc/services-guerilla-marketing'); ?>
                <?php get_template_part('inc/services-pr-marketing'); ?>
                <?php get_template_part('inc/services-product-marketing'); ?>
            </div>
        </article>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/modal-popup'); ?>
<?php //get_sidebar();  ?>
<?php get_footer(); ?>