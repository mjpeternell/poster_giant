<?php
/**
 * Template Name: Contact Page Template
 * The template used for displaying page content on homepage
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php
        // Start the Loop.
        while (have_posts()) : the_post();
        ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php
            if (has_post_thumbnail($post->ID)):
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
            $my_bground = 'style="background-image: url(\'' . $image[0] . '\')"';
            endif;
            ?>

            <div class="hero-subpages hero-bg about-bg-height clayout" <?php echo $my_bground; ?>>
                <div class="hero-img-wrapper ctitle">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                            <h2 class="entry-subtitle"><?php
                                if (get_field('contact_tagline')) {
                                echo get_field('contact_tagline');
                                }
                                ?></h2>
                            <?php
                            if (get_field('contact_phone')) {
                            echo '<div class="pg-phone">' . get_field('contact_phone') . '</div>';
                            }
                            if (get_field('contact_address')) {
                            echo '<div class="pg-address">' . get_field('contact_address') . '</div>';
                            }
                            ?>
                        </div>
                        <div class="col-md-6 cform">
                            <?php
                            $host = $_SERVER['HTTP_HOST'];
                            if($host == "postergiant.dev") {
                                echo do_shortcode('[contact-form-7 id="125" title="Contact form 1"]');
                            }
                            if ($host == "poster.mattpeternell.net" or $host == "www.poster.mattpeternell.net") {
                                echo do_shortcode('[contact-form-7 id="90" title="Contact form 1"]');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
<?php endwhile; ?>
            <section id="servicesList" class="sections section-services-list">
                <div class="row">
                    <div class="entry-content col-md-6 why-we-here">
                        <?php the_content(); ?>
                        <?php //wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
<?php //edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>');  ?>
                    </div><!-- .entry-content -->
                    <div class="entry-content col-md-6"></div>
                </div>
            </section>
        </article>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();    ?>
<?php get_footer(); ?>