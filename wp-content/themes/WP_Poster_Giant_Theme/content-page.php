<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="fluid-container">
        <div class="row-hero">
            <div class="col-page-12"><?php postergiant_post_thumbnail(); ?></div>
        </div>
        <div class="row">
            <div class="col-page-8">    
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php
                    if (get_field('page_subtitle')) {
                        echo '<h2>' . get_field('page_subtitle') . '</h2>';
                    }
                    ?>
                </header><!-- .entry-header -->
                <div class="entry-content">
                    <?php the_content(); ?>
                    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                    <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
                </div><!-- .entry-content -->
            </div>
            <div class="col-page-4">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->


