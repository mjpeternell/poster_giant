<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>

<section id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

        <?php if (have_posts()) : ?>
            <?php if (is_search()) : ?>
                <div class = "single-bg">
                <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
                </div>
                <div class="fluid-container">
                    <div class="row">
                        <header class="page-header">
                            <h1 class="page-title"><?php printf(__('Search Results for: %s', 'pixelfire'), '<span>' . get_search_query() . '</span>'); ?></h1>
                        </header><!-- .page-header -->
                    </div>
                </div>
            <?php endif; ?>


            <?php //pixelfire_content_nav('nav-above'); ?>

            <?php /* Start the Loop */ ?>

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('content', 'search'); ?>
            <?php endwhile; ?>
        
            <div id="myArchiveSearch" class="archive_search">
             <?php get_template_part('searchform'); ?>
            </div>
        <?php else : ?>
            <div class = "single-bg">
                <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
                </div>
                <div class="fluid-container">
                    <div class="row">
                        <header class="page-header">
                            <h1 class="page-title"><?php printf(__('Search Results for: %s', 'pixelfire'), '<span>' . get_search_query() . '</span>'); ?></h1>
                        </header><!-- .page-header -->
                    </div>
                    <div class="row"><?php get_template_part('no-results', 'search'); ?></div>
                </div>
            

        <?php endif; ?>


    </div><!-- #content .site-content -->
</section><!-- #primary .content-area -->


<?php get_footer(); ?>