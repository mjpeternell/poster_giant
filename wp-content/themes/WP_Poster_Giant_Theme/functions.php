<?php
/**
 * WP PosterGiant Theme functions and definitions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * @package WP PosterGiant Theme
 * @since WP PosterGiant Theme 0.1.0
 */
// Useful global constants
define('POSTERGIANT_VERSION', '0.1.0');

/**
 * Add humans.txt to the <head> element.
 */
function postergiant_header_meta() {
    $humans = '<link type="text/plain" rel="author" href="' . get_template_directory_uri() . '/humans.txt" />';

    echo apply_filters('postergiant_humans', $humans);
}

add_action('wp_head', 'postergiant_header_meta');

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since WP PixelFire Theme 1.0
 */
if (!isset($content_width))
    $content_width = 640; /* pixels */

if (!function_exists('postergiant_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which runs
     * before the init hook. The init hook is too late for some features, such as indicating
     * support post thumbnails.
     *
     * @since WP PixelFire Theme 1.0
     */
    function postergiant_setup() {

        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(680, 680, true);
        add_image_size('twentyfourteen-1920x1080', 1920, 1080, true);
        add_image_size('twentyfourteen-680x453', 680, 453, true);
        add_image_size('twentyfourteen-680x680', 680, 680, true);
        add_image_size('twentyfourteen-480x480', 480, 480, true);
        add_image_size('twentyfourteen-680x453', 680, 453, true);
        add_image_size('twentyfourteen-453x680', 453, 680, true);
        add_image_size('twentyfourteen-1024x683', 1024, 683, array('center', 'center'));
        add_image_size('twentyfourteen-768x515', 768, 515, array('center', 'center'));

        /**
         * Custom template tags for this theme.
         */
        require( get_template_directory() . '/inc/template-tags.php' );

        /**
         * Custom functions that act independently of the theme templates
         */
        require( get_template_directory() . '/inc/extras.php' );

        /**
         * Customizer additions
         */
        require( get_template_directory() . '/inc/customizer.php' );

        /**
         * WordPress.com-specific functions and definitions
         */
        //require( get_template_directory() . '/inc/wpcom.php' );

        /**
         * Make theme available for translation
         * Translations can be filed in the /languages/ directory
         */
        load_theme_textdomain('pixelfire', get_template_directory() . '/languages');

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support('automatic-feed-links');

        /**
         * Enable support for Post Thumbnails
         */
        add_theme_support('post-thumbnails');

        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'postergiant'),
        ));

        /**
         * Add support for the Aside Post Formats
         */
        add_theme_support('post-formats', array('aside',));
    }

endif; // postergiant_setup
add_action('after_setup_theme', 'postergiant_setup');

/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since WP PixelFire Theme 1.0
 */
function postergiant_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'postergiant'),
        'id' => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Global Footer Widget 1', 'postergiant'),
        'id' => 'footer-widget-1',
        'before_widget' => '<aside id="%1$s" class="col-md-3 widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Global Footer Widget 2', 'postergiant'),
        'id' => 'footer-widget-2',
        'before_widget' => '<aside id="%1$s" class="col-md-3 widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
    register_sidebar(array(
        'name' => __('Global Footer Widget 3', 'postergiant'),
        'id' => 'footer-widget-3',
        'before_widget' => '<aside id="%1$s" class="col-md-3 widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h1 class="widget-title">',
        'after_title' => '</h1>',
    ));
}

add_action('widgets_init', 'postergiant_widgets_init');

/**
 * Enqueue scripts and styles
 */
function postergiant_scripts() {
    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_script('small-menu', get_template_directory_uri() . '/js/small-menu.js', array('jquery'), '20120206', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (is_singular() && wp_attachment_is_image()) {
        wp_enqueue_script('keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20120202');
    }

    //Check for Dev Environment if true load unminified scripts and css else load minified versions.
    $minified = "";
    //echo $myDomain = $_SERVER['HTTP_HOST'];
    if (($_SERVER['HTTP_HOST'] === 'poster.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'postergiant.net') {
        $minified = ".min";
    } else {
        $minified = "";
    }
    wp_enqueue_style('postergiant-css', get_template_directory_uri() . '/assets/css/theme-style' . $minified . '.css', array(), POSTERGIANT_VERSION);
    wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), POSTERGIANT_VERSION);
    wp_enqueue_script('pixelfire-js-min', get_template_directory_uri() . '/assets/js/wp_postergiant_theme' . $minified . '.js', array('jquery'), POSTERGIANT_VERSION, true);
    wp_enqueue_script('pixelfire-plug', get_template_directory_uri() . '/assets/js/plugins'.$minified.'.js', array('jquery'), POSTERGIANT_VERSION, true);
    wp_enqueue_script('isotope-js', get_template_directory_uri() . '/assets/js/vendor/isotope.pkgd.min.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('packery-js', get_template_directory_uri() . '/assets/js/vendor/packery-mode.pkgd.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('lazy-js', get_template_directory_uri() . '/assets/js/vendor/jquery.lazyload.min.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('succinct-js', get_template_directory_uri() . '/assets/js/vendor/jQuery.succinct.min.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('bx-slider', get_template_directory_uri() . '/assets/js/vendor/jquery.bxslider' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('bx-slider-ease', get_template_directory_uri() . '/assets/js/vendor/jquery.easing.1.3.js', array(), POSTERGIANT_VERSION, true);
    wp_enqueue_script('jquery-vide-js', get_template_directory_uri() . '/assets/js/vendor/jquery.vide' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
  
    if (is_front_page()):
        wp_enqueue_script('parallax-js', get_template_directory_uri() . '/assets/js/vendor/parallax' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
        wp_enqueue_script('isotope-hjs', get_template_directory_uri() . '/assets/js/case-study-isotope-home' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
    endif;
    if (is_page( 'case-studies' )):
        wp_enqueue_script('isotope-cs', get_template_directory_uri() . '/assets/js/case-study-isotope' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
    endif;
    if (is_page( 'about-us' )):
         wp_enqueue_script('isotope-js1', get_template_directory_uri() . '/assets/js/case-study-isotope-about' . $minified . '.js', array(), POSTERGIANT_VERSION, true);
    endif;
}

add_action('wp_enqueue_scripts', 'postergiant_scripts');

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

/* * *******************************************************
 * New function - storefront_site_svg()
 * ****************************************************** */

if (!function_exists('logo_site_svg')) {

    /**
     * Display Site SVG Logo
     * @since  1.0.0
     * @return void
     */
    function logo_site_svg() {
        if (function_exists('jetpack_has_site_logo') && jetpack_has_site_logo()) {
            jetpack_the_site_logo();
        } else {
            ?>
            <div class="site-branding">
                <div class="logo">
                    <?php get_template_part('inc/logo-svg') ?>
                </div>
            </div>
            <?php
        }
    }

    add_action('logo_svg', 'logo_site_svg', 0);
}

add_filter('post_thumbnail_html', 'remove_width_attribute', 10);
add_filter('image_send_to_editor', 'remove_width_attribute', 10);

function remove_width_attribute($html) {
    $html = preg_replace('/(width|height)="\d*"\s/', "", $html);
    return $html;
}

// generate tag cloud
function My_TagCloud($params = array()) {

    extract(shortcode_atts(array(
        'orderby' => 'name', // sort by name or count
        'order' => 'ASC', // in ASCending or DESCending order
        'number' => '', // limit the number of tags
        'wrapper' => '', // a tag wrapped around tag links, e.g. li
        'sizeclass' => 'cs-tag', // the tag class name
        'sizemin' => 1, // the smallest number applied to the tag class
        'sizemax' => 5   // the largest number applied to the tab class
                    ), $params));

    // initialize
    $ret = '';
    $min = 9999999;
    $max = 0;

    // fetch all WordPress tags
    $tags = get_tags(array('orderby' => $orderby, 'order' => $order, 'number' => $number));

    // get minimum and maximum number tag counts
    foreach ($tags as $tag) {
        $min = min($min, $tag->count);
        $max = max($max, $tag->count);
    }

    // generate tag list
    foreach ($tags as $tag) {
        $url = get_tag_link($tag->term_id);
        $title = $tag->count . ' article' . ($tag->count == 1 ? '' : 's');
        if ($max > $min) {
            $class = $sizeclass . floor((($tag->count - $min) / ($max - $min)) * ($sizemax - $sizemin) + $sizemin);
        } else {
            $class = $sizeclass;
        }

        $ret .= ($wrapper ? "<" . $wrapper . ">" : "") . '<a href="#' . str_replace(' ', '-', $tag->name) . '" data-filter=".' . str_replace(' ', '-', $tag->name) . '" class="' . $class . '" title="' . $tag->name . '">' . $tag->name . '</a>' . ($wrapper ? "</" . $wrapper . ">" : "");
    }
    return str_replace(get_bloginfo('url'), '', $ret);
}

add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function prev_posts_link_attributes() {
    return 'class="nav-previous"';
}

function next_posts_link_attributes() {
    return 'class="nav-next"';
}

// Recent Post Sidebar with thumbnail images
function my_recent_posts() {
    ?>
    <aside id="pg_recent_posts" class="widget">
        <?php
        $post_type = get_post_type();

        switch ($post_type) {
            case 'post':
                $rp_post_type = "post";
                echo '<h1 class="widget-title">Recent Posts</h1>';
                break;

            case 'pg-case-studies':

                $rp_post_type = "pg-case-studies";
                echo '<h1 class="widget-title">Recent Case Studies</h1>';
                break;

            case 'services':

                $rp_post_type = "services";
                echo '<h1 class="widget-title">Recent Services</h1>';
                break;

            default:
                $rp_post_type = "post";
                echo '<h1 class="widget-title">Recent Posts</h1>';
        }
        ?>
        <?php
        $args_rp = array(
            'numberposts' => 5,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => $rp_post_type,
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $recent_posts = wp_get_recent_posts($args_rp);
        //print_r($recent_posts);

        echo '<ul class="pg-recent-posts">';
        foreach ($recent_posts as $recent) {
            if ($recent['post_status'] == "publish") {

                if (has_post_thumbnail($recent["ID"])) {
                    echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look ' . esc_attr($recent["post_title"]) . '" class="rp-thumb">' . get_the_post_thumbnail($recent["ID"], 'thumbnail') . '</a>'
                    . '<a href="' . get_permalink($recent["ID"]) . '" title="Look ' . esc_attr($recent["post_title"]) . '" class="rp-link">' . $recent["post_title"] . '<br><span class="date">' . esc_attr(date("F d,Y", strtotime($recent["post_date"]))) . '</span></a>'
                    . '</li> ';
                } else {
                    echo '<li>
		<a href="' . get_permalink($recent["ID"]) . '" title="Look ' . esc_attr($recent["post_title"]) . '" >' . $recent["post_title"] . '</a></li> ';
                }
            }
        }
        echo '</ul>';
        ?></aside>
        <?
}

add_action('before_sidebar', 'my_recent_posts');

//function get_post_page_content($atts) {
//    extract(shortcode_atts(array(
//        'id' => null,
//        'title' => false,
//                    ), $atts));
//
//    $the_query = new WP_Query('page_id=' . $id);
//    while ($the_query->have_posts()) {
//        $the_query->the_post();
//        if ($title == true) {
//            the_title();
//        }
//        the_content();
//    }
//    wp_reset_postdata();
//}
//
//add_shortcode('my_content', 'get_post_page_content');
