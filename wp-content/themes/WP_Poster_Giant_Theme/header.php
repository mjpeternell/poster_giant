<?php
/**
 * The template for displaying the header.
 *
 * @package WP PixelFire Theme
 * @since 0.1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>
        <?php $themeLink = get_stylesheet_directory_uri(); ?>
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $themeLink; ?>/assets/icons/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?php echo $themeLink; ?>/assets/icons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $themeLink; ?>/assets/icons/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo $themeLink; ?>/assets/icons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo $themeLink; ?>/assets/icons/manifest.json">
        <link rel="mask-icon" href="<?php echo $themeLink; ?>/assets/icons//safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#c82027">
        <meta name="msapplication-TileImage" content="<?php echo $themeLink; ?>/assets/icons/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <?php wp_head(); ?>
        <script src="<?php echo $themeLink; ?>/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body <?php body_class(); ?>>
        <?php
        if ($_SERVER['HTTP_HOST'] === 'postergiant.net') {
            get_template_part('inc/google-analytics');
        }
        ?>
        <header class="navigation" role="banner">
            <div class="navigation-wrapper">
                <?php do_action('logo_svg'); ?>
                <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
                <?php postergiant_primary_menu(); ?>
                <div class="header-social-nav">
                    <?php postergiant_social_menu(); ?>
                </div>
            </div>
        </header>
        <div id="page" class="hfeed site">
            <div id="main" class="site-main">