<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php $themeLink = get_stylesheet_directory_uri(); ?>
        <?php if (is_404()) : ?>
            <div class="single-bg">
            <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
            </div>
            <div class="fluid-container">
                <div class="row">
                    <header class="entry-header col-md-12">
                        <h1 class="entry-title"><?php _e('Oops! That page can&rsquo;t be found.', 'pixelfire'); ?></h1>
                    </header>
                </div>
            </div>
        <?php endif; ?>
        <div class="fluid-container">
            <div class="row">
                <div class="col-error-8">
                    <article id="post-0" class="post error404 not-found">
                        <div class="entry-content">
                            <p><?php _e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'pixelfire'); ?></p>
                            <?php get_search_form(); ?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-0 .post .error404 .not-found --> 
                </div>
                <div class="col-error-4">
                    <?php the_widget('WP_Widget_Recent_Posts'); ?>
                    <div class="widget">
                        <h2 class="widgettitle"><?php _e('Most Used Categories', 'pixelfire'); ?></h2>
                        <ul>
                            <?php wp_list_categories(array('orderby' => 'count', 'order' => 'DESC', 'show_count' => 1, 'title_li' => '', 'number' => 10)); ?>
                        </ul>
                    </div><!-- .widget -->
                    <div class="widget">
                        <?php
                        /* translators: %1$s: smilie */
                        $archive_content = '<p>' . sprintf(__('Try looking in the monthly archives. %1$s', 'pixelfire'), convert_smilies(':)')) . '</p>';
                        the_widget('WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content");
                        ?>
                    </div>
                    <div class="widget">
                        <?php the_widget('WP_Widget_Tag_Cloud'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php get_footer(); ?>