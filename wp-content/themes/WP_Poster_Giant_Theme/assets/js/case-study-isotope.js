/*! WP PosterGiant Theme - v1.5.0 - 2016-10-19
 * postergiant.net
 * Copyright (c) 2016; * Licensed GPLv2+ */

function initIsotope() {

    // init Isotope
    var $imgs = jQuery('img.lazy');
    var $grid = jQuery('.grid');

    var $window = jQuery(window);

    $grid.isotope({
        //isInitLayout: false,
        layoutMode: 'packery',
        packery: {
            gutter: '.gutter-sizer'
        },
        itemSelector: '.grid-item',
        percentPosition: true,
        isInitLayout: true
    });

    $imgs.lazyload({
        load: function () {
            $grid.isotope('layout');
        }
    });


    // bind filter button click
    jQuery('#cities li, #segments li').on('click', 'a', function () {
        var filterValue = jQuery(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });

    // bind Grid item Category List filter <a> click
    jQuery('.cat-item-set').on('click', 'a', function () {
        var filterValue = jQuery(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });


    // change is-checked class on buttons
    jQuery('.option-set').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

    // change is-checked class on buttons
    jQuery('.cat-item-set .cat-item').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

    jQuery('#mobileFilterBtn').on('click', function (e) {
        e.preventDefault();
        //jQuery( "#filterMenu" ).toggle( 300,'linear');
        jQuery(".grid-controls").toggleClass('showFilter').animate({"right": "0px"}, 300);
        jQuery(".cat-child a").click(function () {
            jQuery("#filterMenu").hide("fast").removeClass('showFilter');
        });
    });

    jQuery('#reset-btn').click(function () {
        //jQuery("#filterMenu").hide("fast");
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });
}

jQuery(window).on('load', function () {
    initIsotope();
});
 