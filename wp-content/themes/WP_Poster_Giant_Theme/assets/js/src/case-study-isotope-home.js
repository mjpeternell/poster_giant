function initIsotope() {
    
    // init Isotope
    var $imgs = jQuery('img.lazy');
    var $grid = jQuery('.grid');

    var $window = jQuery(window);
    $grid.isotope({
        itemSelector: '.grid-item',
        masonry: {
            columnWidth: 0
        }
    });


    $imgs.lazyload({
        load: function () {
            $grid.isotope('layout');
        }
    });

    // bind filter button click
    jQuery('#segments li, #segments li .reset').on('click', 'a', function () {
        var filterValue = jQuery(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });

    // change is-checked class on buttons
    jQuery('.option-set, .cs-tax-name').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

    // change is-checked class on buttons
    jQuery('.cat-item-set .cat-item').each(function (i, buttonGroup) {
        var $buttonGroup = jQuery(buttonGroup);
        $buttonGroup.on('click', 'a', function () {
            $buttonGroup.find('.selected').removeClass('selected');
            jQuery(this).addClass('selected');
        });
    });

    jQuery('#mobileFilterBtn').on('click', function (e) {
        e.preventDefault();
        jQuery(".grid-controls").toggleClass('showFilter').animate({"right": "0px"}, 300);
        jQuery(".cat-child a").click(function () {
            jQuery("#filterMenu").hide("fast").removeClass('showFilter');
        });
    });

    jQuery('#reset-btn').click(function () {
        //jQuery("#filterMenu").hide("fast");
        $imgs.lazyload({
            load: function () {
                $grid.isotope('layout');
            }
        });
    });
}
jQuery(window).on('load', function () {

    initIsotope();

    jQuery('#filters li').on('click', 'a', function () {
        //jQuery('html, body, #caseStudies').animate({scrollTop : yPo},800);
        jQuery('html, body, #caseStudies').animate({scrollTop: jQuery("#caseStudies").offset().top}, 200);

    });
    jQuery('#filters .reset').on('click', 'a', function () {
        jQuery('html, body, #caseStudies').animate({scrollTop: jQuery("#caseStudies").offset().top}, 200);
    });



});
 