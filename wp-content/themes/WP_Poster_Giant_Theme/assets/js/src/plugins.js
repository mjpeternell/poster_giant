

(function () {
    var d = document, accordionToggles = d.querySelectorAll('.js-accordionTrigger'), setAria, setAccordionAria, switchAccordion, touchSupported = ('ontouchstart' in window), pointerSupported = ('pointerdown' in window);
    var skipClickDelay, setAriaAttr;

    skipClickDelay = function (e) {
        e.preventDefault();
        e.target.click();
    };

    setAriaAttr = function (el, ariaType, newProperty) {
        el.setAttribute(ariaType, newProperty);
    };
    setAccordionAria = function (el1, el2, expanded) {
        switch (expanded) {
            case "true":
                setAriaAttr(el1, 'aria-expanded', 'true');
                setAriaAttr(el2, 'aria-hidden', 'false');
                break;
            case "false":
                setAriaAttr(el1, 'aria-expanded', 'false');
                setAriaAttr(el2, 'aria-hidden', 'true');
                break;
            default:
                break;
        }
    };
//function
    switchAccordion = function (e) {
        console.log("triggered");
        e.preventDefault();
        var thisAnswer = e.target.parentNode.nextElementSibling;
        var thisQuestion = e.target;
        if (thisAnswer.classList.contains('is-collapsed')) {
            setAccordionAria(thisQuestion, thisAnswer, 'true');
        } else {
            setAccordionAria(thisQuestion, thisAnswer, 'false');
        }
        thisQuestion.classList.toggle('is-collapsed');
        thisQuestion.classList.toggle('is-expanded');
        thisAnswer.classList.toggle('is-collapsed');
        thisAnswer.classList.toggle('is-expanded');

        thisAnswer.classList.toggle('animateIn');
    };
    for (var i = 0, len = accordionToggles.length; i < len; i++) {
        if (touchSupported) {
            accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
        }
        if (pointerSupported) {
            accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
        }
        accordionToggles[i].addEventListener('click', switchAccordion, false);
    }
})();

// Instagram Feed API
// Instagram Feed
jQuery.ajax({
    type: "GET",
    dataType: "jsonp",
    cache: false,
    url: "https://api.instagram.com/v1/users/self/media/recent/?access_token=296604218.1bcedd5.79ac9e5664764e57909cafbd9ae6dc22",
    success: function (data) {
        // placing the images on the page

        for (var i = 0; i < 12; i++) {
            var loca = "";
            if (data.data[i].location !== null) {
                if (data.data[i].location.name !== null) {
                    loca = " - " + data.data[i].location.name;
                } else {
                    loca = "";
                }
            }

            var capt = "";
            if (data.data[i].caption !== null) {
                if (data.data[i].caption.text !== null) {
                    var regexUnderscore = new RegExp(".", "g"); //indicates global match
                    var regexHash = new RegExp("#", "g");
                    capt = data.data[i].caption.text;
                    //capt.replace(/'/g, '');
                    capt.replace(regexHash, "").replace(regexUnderscore, "");
                } else {
                    capt = "";
                }
            }
            jQuery(".insta-feed").append("<li class='fade'><a target='_blank' href=" + data.data[i].link + " title=\"Visit the posterGIANT Instagram page to like or comment.\"><img src='" + data.data[i].images.thumbnail.url + "'></img></a></li>");
        }
    }
});