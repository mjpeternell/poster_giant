jQuery(document).ready(function () {
//uses classList, setAttribute, and querySelectorAll
//if you want this to work in IE8/9 youll need to polyfill these
//Twitter Parsers
//Twitter Parsers
String.prototype.parseURL = function() {
	return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9\-_]+\.[A-Za-z0-9\-_:%&~\?\/.=]+/g, function(url) {
		return url.link(url);
	});
};
String.prototype.parseUsername = function() {
	return this.replace(/[@]+[A-Za-z0-9\-_]+/g, function(u) {
		var username = u.replace("@","");
		return u.link("http://twitter.com/"+username);
	});
};
String.prototype.parseHashtag = function() {
	return this.replace(/[#]+[A-Za-z0-9\-_]+/g, function(t) {
		var tag = t.replace("#","%23");
		return t.link("http://search.twitter.com/search?q="+tag);
	});
};
function parseDate(str) {
    var v=str.split(' ');
    return new Date(Date.parse(v[1]+" "+v[2]+", "+v[5]+" "+v[3]+" UTC"));
} 

function loadLatestTweet(){
    var numTweets = 1;
    var _url = 'https://api.twitter.com/1/statuses/user_timeline/CypressNorth.json?callback=?&count='+numTweets+'&include_rts=1';
    $.getJSON(_url,function(data){
    for(var i = 0; i< data.length; i++){
            var tweet = data[i].text;
            var created = parseDate(data[i].created_at);
            var createdDate = created.getDate()+'-'+(created.getMonth()+1)+'-'+created.getFullYear()+' at '+created.getHours()+':'+created.getMinutes();
            tweet = tweet.parseURL().parseUsername().parseHashtag();
            tweet += '<div class="tweeter-info"><div class="uppercase bold"><a href="https://twitter.com/#!/CypressNorth" target="_blank" class="black">@CypressNorth</a></div><div class="right"><a href="https://twitter.com/#!/CypressNorth/status/'+data[i].id_str+'">'+createdDate+'</a></div></div>';
            $("#twitter-feed").append('<p>'+tweet+'</p>');
        }
    });
}

//Resize Window
    function sliderResize() {
        //My Window Height detections
        var myWindow_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var myWindow_h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        if (myWindow_w <= 768) {
            jQuery('.computer .outer-wrapper, .computer .hero').css({height: 'inherit', width: 'inherit'});
            console.log("H: " + myWindow_h + " W: " + myWindow_w);
        } else {
            console.log("H: " + myWindow_h + " W: " + myWindow_w);
            jQuery('.hero.parallax-window').css({height: myWindow_h - 15, width: "auto"});
        }
    }

    jQuery(window).resize(function () {
        sliderResize();
    });

    jQuery(window).load(function () {
        sliderResize();
    });

    // Listen for orientation changes
    window.addEventListener("orientationchange", function () {
        sliderResize();
    }, false);


//    var $root =  jQuery('html, body');
//    jQuery('.os-box a').click(function () {
//        $root.animate({
//            scrollTop: jQuery( $.attr(this, 'href')).offset().top
//        }, 500);
//        return false;
//    });
//    var $root = jQuery('html, body');
//    jQuery('.os-box a').click(function () {
//        var href = jQuery.attr(this, 'href');
//        $root.animate({
//            scrollTop: jQuery(href).offset().top
//        }, 500, function () {
//            window.location.hash = href;
//        });
//        return false;
//    });

// to top right away
    if (window.location.hash) {
        scroll(0, 0);
    }
// void some browsers issue
    setTimeout(function () {
        scroll(0, 0);
    }, 1);

    jQuery(function () {
        var $root = jQuery('html, body');
        // your current click function
//        jQuery('.os-box a').on('click', function (e) {
//            e.preventDefault();
//            jQuery('html, body').animate({
//                scrollTop: jQuery(jQuery(this).attr('href')).offset().top + 'px'
//            }, 1000, 'swing');
//        });

        // *only* if we have anchor on the url
        if (window.location.hash) {

            // smooth scroll to the anchor id
            $root.animate({
                scrollTop: jQuery(window.location.hash).offset().top - 200 + 'px'
            }, 1000, 'swing');
        }
    });


    jQuery('.isotope-item .grid-item-title a').succinct({
        size: 24
    });
    jQuery('.isotope-item .entry-content p').succinct({
        size: 150
    });
    jQuery('.isotope-item .grid-item-job-excerpt').succinct({
        size: 400
    });

    jQuery('.testimonial-slider').bxSlider({
        auto: true,
        controls: false,
        mode: 'fade',
        useCSS: true,
        infiniteLoop: true,
        easing: 'swing',
        speed: 200,
        pause: 6000
    });


    jQuery('.grid.about .inner-content').hide();
    var all_spans = jQuery('.grid.about .isotope-item').parent().find('.inner-content');

    jQuery('.grid.about .learn-more').click(function (e) {
        //hide all span
        all_spans.hide();
        var xthis = jQuery(this).parent().find('.inner-content');
        var current = jQuery(this).parent().find('.content');

        var isVisible = all_spans.is(':visible');
        if (isVisible === true) {
            jQuery(this).parent().find('.inner-content').hide();

        } else {
            jQuery(this).parent().find('.inner-content').show();
        }

        jQuery(".inner-content").delay(9000).fadeOut(300);
        e.preventDefault();
    });

    jQuery('.grid.about .desktop-link').hover(function (e) {
        //hide all span
        all_spans.hide();
        var xthis = jQuery(this).parent().find('.inner-content');
        var current = jQuery(this).parent().find('.content');

        var isVisible = all_spans.is(':visible');
        if (isVisible === true) {
            jQuery(this).parent().find('.inner-content').hide();

        } else {
            jQuery(this).parent().find('.inner-content').show();
        }

        //jQuery(".inner-content").delay(30000).fadeOut(300);
        e.preventDefault();
    });

    jQuery('.hov').hover(
            function () {
                jQuery(this).parent().addClass('rossa');
            },
            function () {
                jQuery(this).parent().removeClass('rossa');
            });


    //Scroll to top of page
    jQuery(window).scroll(function () {
        var my_docHeight = jQuery(document).height();
        var my_winHeight = jQuery(window).height();
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
        if (jQuery(this).scrollTop() + my_winHeight === my_docHeight) {
            jQuery('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    jQuery('.scroll-to-top').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    var menuToggle = jQuery("#js-mobile-menu").unbind();
    jQuery("#js-navigation-menu").removeClass("show");

    menuToggle.on("click", function (e) {
        e.preventDefault();
        jQuery('.social-list').show();
        jQuery("#js-navigation-menu").slideToggle(function () {
            if (jQuery("#js-navigation-menu").is(":hidden")) {
                jQuery("#js-navigation-menu").removeAttr("style");
                jQuery('.social-list').hide();
            }
        });
    });

    jQuery('#js-navigation-menu > li').not("ul li ul li").addClass('nav-link');
    jQuery('#js-navigation-menu li ul').parent().addClass('nav-link more').attr('id', 'js-navigation-more');
    jQuery('#js-navigation-menu li ul').addClass('submenu');


    jQuery('a[name=\'modal\']').click(function (e) {

        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = jQuery(this).attr('href');

        //Get the window height and width
        var winH = jQuery(window).height();
        var winW = jQuery(window).width();
        console.log(winH + ' - ' + winW);
        //Set the popup window to center
        if (winW <= 800) {
            jQuery(id).css('top', 10);
        } else {
            //jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
            jQuery(id).css('top', 50);
        }
        //jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        jQuery(id).css('left', winW / 2 - jQuery(id).width() / 2);

        //var theTitle = jQuery(this).siblings(".entry-header").children('h1').find('a').text();
        var theTitle = jQuery(this).attr("data-img-title");
        var theContent = '<img src="' + jQuery(this).attr("data-pop-img") + '" class="pop-image"/>';
        console.log(theTitle);
        console.log(theContent);
        jQuery(id).find('header h4').append(theTitle);
        jQuery(id).find('.content').append(theContent);

        //transition effect
        jQuery('#modal_screen').fadeIn(500);
        jQuery(id).fadeIn(500);

    });

//if close button is clicked
    jQuery('.modalwindow .close-btn').click(function (e) {
        console.log(e);
        var mod_id = jQuery('.modalwindow');
        jQuery('.modalwindow').find('header h4').empty();
        jQuery('.modalwindow').find('.content .pop-image').remove();
        //Cancel the link behavior
        e.preventDefault();

        jQuery('.modalwindow').fadeOut(500);
        jQuery('#modal_screen').fadeOut(500);
    });
});

