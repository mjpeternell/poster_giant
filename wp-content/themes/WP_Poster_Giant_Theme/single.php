<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
 <?php $themeLink = get_stylesheet_directory_uri(); ?>
        <?php if (is_single()) : ?>
            <div class = "single-bg">
            <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
            </div>
            <div class="fluid-container">
                <div class="row">
                    <header class="entry-header col-md-12">
                        <h1 class="entry-title">Blog</h1>
                    </header>
                </div>
            </div>
        <?php endif; ?>

        <?php while (have_posts()) : the_post(); ?>

            <?php //pixelfire_content_nav('nav-above'); ?>

            <?php get_template_part('content', 'single'); ?>

            <?php pixelfire_content_nav('nav-below'); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            //if ( comments_open() || '0' != get_comments_number() )
            //comments_template( '', true );
            ?>

        <?php endwhile; // end of the loop. ?>

    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->


<?php get_footer(); ?>