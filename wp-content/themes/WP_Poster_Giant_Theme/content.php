<?php
/**
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('child-row'); ?>>
    <div class="col-arch-9">
        <header class="entry-header">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'pixelfire'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
            <?php if (('pg-case-studies' == get_post_type()) || (('post' == get_post_type()))) : ?>
                <div class="entry-meta">
                    <?php //pixelfire_posted_on(); ?>
                    <time class="entry-date"><?php echo get_the_date(); ?></time>
                    <?php //the_date('M d, Y', '<time class="entry-date">', '</time>'); ?>
                </div><!-- .entry-meta -->
            <?php endif; ?>
        </header><!-- .entry-header -->

        <?php if (is_search()) : // Only display Excerpts for Search ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div><!-- .entry-summary -->
        <?php else : ?>
            <div class="entry-content">
                <?php //the_excerpt(); ?>
                   
                <?php 
                $content = get_the_excerpt(); 
                $link = get_the_permalink();
                $rm_link = '<a href="'.$link .'" class="page-links">Read more </a>';
                //echo mb_strimwidth($content, 0, 300, '...'.$rm_link.'');   
                echo $content;
                ?>
                <?php //the_content(__('Continue reading <span class="meta-nav">&rarr;</span>', 'pixelfire')); ?>
                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
                
            </div><!-- .entry-content -->
        <?php endif; ?>

        <footer class="entry-meta">
            <?php if ('post' == get_post_type()) : // Hide category and tag text for pages on Search ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(',', 'pixelfire'));
                if ($categories_list && pixelfire_categorized_blog()) :
                    ?>
                    <span class="cat-links">
                        <?php printf(__('Posted in %1$s', 'pixelfire'), $categories_list); ?>
                    </span>
                <?php endif; // End if categories ?>

                <?php
                /* translators: used between list items, there is a space after the comma */
                $tags_list = get_the_tag_list('', __(', ', 'pixelfire'));
                if ($tags_list) :
                    ?>
                    <span class="sep"> | </span>
                    <span class="tags-links">
                        <?php printf(__('Tagged %1$s', 'pixelfire'), $tags_list); ?>
                    </span>
                <?php endif; // End if $tags_list ?>
            <?php endif; // End if 'post' == get_post_type() ?>

            <?php if (!post_password_required() && ( comments_open() || '0' != get_comments_number() )) : ?>
<!--                <span class="sep"> | </span>
                <span class="comments-link"><?php comments_popup_link(__('Leave a comment', 'pixelfire'), __('1 Comment', 'pixelfire'), __('% Comments', 'pixelfire')); ?></span>-->
            <?php endif; ?>

            <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="sep"> | </span><span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
    <div class="col-arch-3">
        <?php if (has_post_thumbnail()) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_post_thumbnail('twentyfourteen-680x680'); ?>
            </a>
        <?php endif; ?>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
