<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
?>

</div><!-- #main .site-main -->


</div><!-- #page .hfeed .site -->
<footer class="footer" role="contentinfo">
    <!--    <div class="footer-logo">
            <img src="/wp-content/themes/WP_Poster_Giant_Theme/assets/images/PIXELFIRE_logo_onBlackHoriz.png" alt="Logo image">
        </div>-->
    <div class="footer-links">
        <div id="FooterNav" class="col-md-3">
            <?php if (has_nav_menu('primary')) : ?>
                <nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e('Footer Primary Menu', 'twentysixteen'); ?>">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'menu_class' => 'primary-menu',
                    ));
                    ?>
                </nav><!-- .main-navigation -->
            </div>
        <?php endif; ?>

        <!-- Three columns of text below the carousel -->
        <?php
        /* Product footer sidebar */
        if (!is_404()) :
            ?>
            <?php if (is_active_sidebar('footer-widget-1')) : ?>
                <?php dynamic_sidebar('footer-widget-1'); ?>

            <?php endif; ?>

            <?php if (is_active_sidebar('footer-widget-2')) : ?>
                <?php dynamic_sidebar('footer-widget-2'); ?>

            <?php endif; ?>

            <?php if (is_active_sidebar('footer-widget-3')) : ?>
                <?php dynamic_sidebar('footer-widget-3'); ?>

            <?php endif; ?>
            <!-- #footer-widgets -->
        <?php endif; ?>
    </div>
    <div class="site-info" role="contentinfo">
        <div class="col-md-12">
            <p>Copyright © <?php echo date("Y"); ?> | posterGIANT LLC | All Rights Reserved </p>
        </div>
    </div><!-- .site-info -->
</footer>

<div class="scroll-to-top" style="display: block;">
    <i class="fa fa-angle-up"></i>
</div>
<?php wp_footer(); ?>
</body>
</html>