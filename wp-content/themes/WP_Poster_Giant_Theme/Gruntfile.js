module.exports = function (grunt) {
// Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            wp_postergiant_theme_1: {
                src: 'assets/js/src/wp_postergiant_theme.js',
                dest: 'assets/js/wp_postergiant_theme.js'
            },
            wp_postergiant_theme_2: {
                src: 'assets/js/src/plugins.js',
                dest: 'assets/js/plugins.js'
            },
            wp_postergiant_theme_3: {
                src: 'assets/js/src/case-study-isotope-home.js',
                dest: 'assets/js/case-study-isotope-home.js'
            },
            wp_postergiant_theme_4: {
                src: 'assets/js/src/case-study-isotope-about.js',
                dest: 'assets/js/case-study-isotope-about.js'
            },
            wp_postergiant_theme_5: {
                src: 'assets/js/src/case-study-isotope.js',
                dest: 'assets/js/case-study-isotope.js'
            }
        },
//        shell: {
//            options: {
//                stderr: false
//            },
//            target: {
//                command: 'cp -a ~/Sites/rooster/wp-content/rooster-park-theme-source/release/wp_pixelfire_theme ~/Sites/rooster/wp-content/themes/'
//            }
//        },
        jshint: {
            all: [
                'Gruntfile.js',
                'assets/js/src/**/*.js',
                'assets/js/test/**/*.js'
            ],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                globals: {
                    exports: true,
                    module: false,
                    $: false,
                    jQuery: false,
                    console: false,
                    document: false,
                    window: false,
                    alert: false,
                    browser: true,
                    scroll: false,
                    setTimeout: false

                }
            }
        },
        uglify: {
            options: {
                mangle: false,
                separator: '\n\n',
                report: 'min',
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            my_target_1: {
                files: {
                    'assets/js/wp_postergiant_theme.min.js': ['assets/js/wp_postergiant_theme.js']
                }
            },
            my_target_2: {
                files: {
                    'assets/js/plugins.min.js': ['assets/js/plugins.js']
                }
            },
            my_target_3: {
                files: {
                    'assets/js/case-study-isotope.min.js': ['assets/js/case-study-isotope.js']
                }
            },
            my_target_4: {
                files: {
                    'assets/js/case-study-isotope-home.min.js': ['assets/js/case-study-isotope-home.js']
                }
            },
            my_target_5: {
                files: {
                    'assets/js/case-study-isotope-about.min.js': ['assets/js/case-study-isotope-about.js']
                }
            }
        },
        test: {
            files: ['assets/js/test/**/*.js']
        },
        sass: {
            all: {
                files: {
                    'assets/css/theme-style.css': 'sass/theme-style.scss'
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            target: {
                expand: true,
                cwd: 'assets/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'assets/css/',
                ext: '.min.css'
            }
        },
        watch: {
            sass: {
                files: ['sass/**/*.scss'],
                tasks: ['sassy'],
                options: {
                    debounceDelay: 500
                }
            },
            scripts: {
                files: [
                    'assets/js/src/**/*.js',
                    'assets/js/case-study-isotope.js',
                    'assets/js/case-study-isotope-about.js',
                    'assets/js/case-study-isotope-home.js'
                ],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    debounceDelay: 500
                }
            }
        },
        clean: {
            main: ['release/<%= pkg.name %>']
        },
        copy: {
            // Copy the plugin to a versioned release directory
            main: {
                src: [
                    '**',
                    '!bower_components/**',
                    '!node_modules/**',
                    '!nbproject/**',
                    '!release/**',
                    '!.git/**',
                    '!css/src/**',
                    '!js/src/**',
                    '!img/src/**',
                    '!Gruntfile.js',
                    '!package.json',
                    '!.gitignore',
                    '!.gitmodules'
                ],
                dest: 'release/<%= pkg.name %>/'
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'zip',
                    archive: './release/<%= pkg.name %>.<%= pkg.version %>.zip'
                },
                expand: true,
                cwd: 'release/<%= pkg.name %>/',
                src: ['**/*'],
                dest: '<%= pkg.name %>/'
            }
        }
    });
    // Load other tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-shell');
    // Default task.
    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'sass', 'cssmin']);
    //Add 'shell' to 'Build" after building theme from grunt-init
    grunt.registerTask('build', ['default', 'clean', 'copy', 'compress']);
    grunt.registerTask('sassy', ['sass', 'cssmin']);
    grunt.util.linefeed = '\n';
};