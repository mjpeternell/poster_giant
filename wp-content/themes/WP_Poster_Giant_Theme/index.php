<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
get_header();
?>
<?php global $wp_query; ?>
<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
        <?php $themeLink = get_stylesheet_directory_uri(); ?>
        <?php if (isset($wp_query) && (bool) $wp_query->is_posts_page) : ?>
            <div class = "single-bg">
            <!--<img src = "<?php echo bloginfo('template_directory') . '/assets/images/PosterGIANT_case_studies_bground.jpg'; ?>" class = "img-responsive"/> -->
            </div>
            <div class="fluid-container">
                <div class="row">
                    <header class="entry-header col-md-12 cat-menu">
                        <h1 class="entry-title">Blog</h1>
<!--                        <div class="blog-cats">
                            <ul class="post-categories">
                                <?php //wp_list_categories(array('title_li' => '')); ?>
                            </ul>
                        </div>-->
                    </header>
                </div>
            </div>
        <?php endif; ?>
        <div class="fluid-container">
            <?php if (have_posts()) : ?>

                <?php //pixelfire_content_nav('nav-above');  ?>

                <?php /* Start the Loop */ ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="row">
                        <div class="col-arch-12 white-bg">
                            <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to overload this in a child theme then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part('content', get_post_format());
                            ?>
                        </div>
                    </div>
                <?php endwhile; ?>

                <?php pixelfire_content_nav('nav-below'); ?>

            <?php else : ?>

                <?php get_template_part('no-results', 'index'); ?>

            <?php endif; ?>
        </div>
    </div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php //get_sidebar();  ?>
<?php get_footer(); ?>