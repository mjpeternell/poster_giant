<?php
/**
 * @package WP PixelFire Theme
 * @since WP PixelFire Theme 1.0
 */
/* translators: used between list items, there is a space after the comma */
$category_list = get_the_category_list(__(', ', 'pixelfire'));

/* translators: used between list items, there is a space after the comma */
$tag_list = get_the_tag_list('', __(', ', 'pixelfire'));
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--    <div class="entry-hero">
        <?php //postergiant_post_thumbnail(); ?>
    </div>-->
    <div class="row white-bg ">
        <header class="col-md-12">
            <h1 class="h1"><?php the_title(); ?></h1>
            <div class="entry-meta">
                <?php 
                pixelfire_posted_on();
                echo '<span class="cat-list">Categories: '.$category_list.'</span>';?>
            </div>
        </header><!-- .entry-header -->
        <div class="col-md-8 entry-content">    
            <?php the_content(); ?>
            <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'pixelfire'), 'after' => '</div>')); ?>
            <footer class="entry-meta">
                <?php

                if (!pixelfire_categorized_blog()) {
                    // This blog only has 1 category so we just need to worry about tags in the meta text
                    if ('' != $tag_list) {
                        $meta_text = __('This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pixelfire');
                    } else {
                        //$meta_text = __('Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pixelfire');
                    }
                } else {
                    // But this blog has loads of categories so we should probably display them here
                    if ('' != $tag_list) {
                        $meta_text = __('This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pixelfire');
                    } else {
                        $meta_text = __('This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'pixelfire');
                    }
                } // end check for categories on this blog

//                printf(
//                        $meta_text, $category_list, $tag_list, get_permalink(), the_title_attribute('echo=0')
//                );
                ?>

                <?php edit_post_link(__('Edit', 'pixelfire'), '<span class="edit-link">', '</span>'); ?>
            </footer><!-- .entry-meta -->
        </div><!-- .entry-content -->

        <div class="col-md-4">
            <?php get_sidebar(); ?>
        </div>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
